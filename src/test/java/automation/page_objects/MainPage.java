package automation.page_objects;

import automation.framework.AbstractPage;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.appium.java_client.pagefactory.iOSFindAll;
import org.openqa.selenium.support.FindBy;
import io.appium.java_client.MobileElement;

public class MainPage extends AbstractPage{

    @AndroidFindBy(id = "main_search")
    @iOSFindAll({
            @iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIASearchBar[1]"),
            @iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIASearchBar[1]")
    })
    public MobileElement searchField;

    public void simpleStep() {
        searchField.click();
        searchField.sendKeys("abcd");
    }
}
