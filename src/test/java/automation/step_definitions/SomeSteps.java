package automation.step_definitions;

import automation.page_objects.MainPage;
import cucumber.api.PendingException;
import cucumber.api.java.en.When;

/**
 * Created by dzmitry_klokau on 11/23/16.
 */
public class SomeSteps {

    private MainPage mainpage = new MainPage();

    @When("^I run a step$")
    public void i_run_a_step() throws Throwable {
        mainpage.simpleStep();
        System.out.println("Hello world");
    }
}
