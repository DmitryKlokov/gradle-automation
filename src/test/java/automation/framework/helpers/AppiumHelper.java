package automation.framework.helpers;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import java.util.concurrent.TimeUnit;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.DesiredCapabilities.*;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

public class AppiumHelper extends EventFiringWebDriver {

    //private static final long DRIVER_WAIT_TIME = Long.parseLong(PropertyManager.getProperty("Driver_wait_time"));
    private static AppiumDriver DRIVER = null;

    static {
        startAppiumDriver();
    }


    private AppiumHelper() {
        super(DRIVER);
    }

    private static void startAppiumDriver() {
        DesiredCapabilities capabilities = null;
        switch (PropertyManager.getProperty("executionOS")) {
            case "ANDROID": {
                File classpathRoot = new File(System.getProperty("user.dir"));
                File appDir = new File(classpathRoot, PropertyManager.getProperty("appDir"));
                File app = new File (appDir, PropertyManager.getProperty("app"));
                capabilities = DesiredCapabilities.iphone();
                capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, PropertyManager.getProperty("platformName"));
                capabilities.setCapability(MobileCapabilityType.DEVICE_NAME , PropertyManager.getProperty("deviceName"));
                capabilities.setCapability(MobileCapabilityType.APP , app.getAbsolutePath());
                capabilities.setCapability("appPackage", PropertyManager.getProperty("appPackage"));
                capabilities.setCapability("appActivity", PropertyManager.getProperty("appActivity"));
                try {
                    DRIVER = new AndroidDriver(new URL(PropertyManager.getProperty("appiumURL")), capabilities);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                break;
            }
            case "IOS": {
                File classpathRoot = new File(System.getProperty("user.dir"));
                File appDir = new File(classpathRoot, PropertyManager.getProperty("appDir"));
                File app = new File(appDir, PropertyManager.getProperty("app"));
                capabilities = DesiredCapabilities.android();
                capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, PropertyManager.getProperty("platformName"));
                capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, PropertyManager.getProperty("deviceName"));
                capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, PropertyManager.getProperty("platformVersion"));
                capabilities.setCapability(MobileCapabilityType.APP, app.getAbsolutePath());
                try {
                    DRIVER = new IOSDriver(new URL(PropertyManager.getProperty("appiumURL")), capabilities);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
        DRIVER.manage().timeouts().implicitlyWait(160, TimeUnit.SECONDS);
    }

    public static AppiumDriver getDriver() {
        return DRIVER;
    }

    public static void closeAppiumDriver(){
        if (DRIVER != null) {
            DRIVER.quit();
            DRIVER = null;
        }
    }
}
