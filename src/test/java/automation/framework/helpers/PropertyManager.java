package automation.framework.helpers;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

final class PropertyManager {
    private static Properties properties;

    static {
        try {
            load();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private PropertyManager() {
    }


    private static void load() throws IOException {
        properties = new Properties();
        String mainPropFileName =   "main.properties";
        try{
            InputStream inputStream = PropertyManager.class.getClassLoader().getResourceAsStream(mainPropFileName);
            properties.load(inputStream);
            switch(getProperty("executionOS")){
                case "ANDROID":{
                    inputStream = PropertyManager.class.getClassLoader().getResourceAsStream(getProperty("androidPropFileName"));
                    properties.load(inputStream);
                    break;
                }
                case "IOS":{
                    inputStream = PropertyManager.class.getClassLoader().getResourceAsStream(getProperty("iosPropFileName"));
                    properties.load(inputStream);
                    break;
                }
                default: {
                    throw new IOException();
                }
            }
        }
        finally {

        }
    }

    static String getProperty(final String key) {
        final String propertyValue = properties.getProperty(key);
        return propertyValue != null ? propertyValue : "Requested property does not exist";
    }
}
