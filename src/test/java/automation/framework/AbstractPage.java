package automation.framework;

import automation.framework.helpers.AppiumHelper;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.AppiumDriver;

import java.util.concurrent.TimeUnit;

public abstract class AbstractPage {

    protected AbstractPage(){
        AppiumDriver driver = AppiumHelper.getDriver();
        //this.wait = new WebDriverWait(androidDriver, AndroidHelper.getDriverWaitTime());
        PageFactory.initElements(new AppiumFieldDecorator(driver, 5, TimeUnit.SECONDS), this);
    }
}
