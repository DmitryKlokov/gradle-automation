package automation;

import automation.framework.helpers.AppiumHelper;
import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.BeforeClass;

@RunWith(Cucumber.class)
@CucumberOptions(
        format = { "json:target/REPORT_NAME.json", "pretty",
                "html:target/HTML_REPORT_NAME" },
        features = "src/test/resources/features"
        ,glue={"automation/step_definitions"}
)

public class testRunner {
    @BeforeClass
    public static void init()
    {
    }

    @AfterClass
    public static void teatDown()
    {
        AppiumHelper.closeAppiumDriver();
    }
}